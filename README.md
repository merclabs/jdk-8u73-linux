# Java SE Development Kit 8 (v8u73) RPM 64bit


### Changing to Oracle's JDK's on Fedora 23 after install of this RPM

Though I've been a member of the JCP for awhile, and loyal to the Red Hat/Fedora
Platform since Red Hat v1.0 when you could buy the boxed version in stores. Like me,
you may not be a fan of the OpenJDK installing as the default JRE on Fedora 23.

In the past, I found myself symlinking and hacking at the file system to get things
to work the way I wanted, not to mention how to get the JDK installation file from the then Sun/ now Oracle website onto my server or cloud instance.

The process of clicking the accept radio button is getting old. Then having to upload the same file your downloaded to your server, consumes valuable time that most don't have. There needs to
be a better way to get the Official release of Java SE JDK XXX on your server in the cloud, which may be bare-bones when spun up.

Currently, Fedora 23 ships (downloads) with OpenJDK as the default JRE:

for i386:

`/usr/lib`

`user/lib/jvm/java-1.*.0-openjdk-1.6.0.0.x86_64/jre/lib/`

for x86_64:

`/usr/lib64`

`usr/lib/jvm/java-1.*.0-openjdk-1.6.0.0.x86_64/jre/amd64/`

*Note: The OpenJDK's package name is: *java-1.?.0-openjdk*


### The Alternative System

For those who are looking for a new way, checkout the *Alternative System*, on your Fedora 23 system as `alternatives`, with related files located in the `/etc/alternatives` directory. This program is Fedora's way of switching JRE Implementations, as well as other software packages (see: `man alternatives` for more information). Though you still have to find a way of getting the installation file to your server (FTP, SCP), which is why I created this repo.

To use this repo, simply clone it, then run `sudo dnf install jdk-8u73-linux-x64.rpm` to install the JDK RPM Package and depends. After installing, you can run the following command to tell your system's environment use the Oracle JDK which is located in `/usr/java/[version]` and symlinked at `/usr/java/latest`:



`alternatives --config java`


Once the menu appears on the screen, you can select your desired JDK implementation by number. Then the following command can be used to view the changes to the system.

`alternatives --display java`

 You can also confirm your changes by checking the version of the JDK installed.

`java -version`   

If you see the version of the JDK you installed, then you should be all set.

`java version "1.8.0_73"`

`Java(TM) SE Runtime Environment (build 1.8.0_73-b02)`

`Java HotSpot(TM) 64-Bit Server VM (build 25.73-b02, mixed mode)`


### More Information
For more information on this topic, go to: [https://fedoraproject.org/wiki/java](https://fedoraproject.org/wiki/java)


### What's in this Repo?
This repo contains the Oracle RPM for Fedora/RedHat/CentOS platforms. I created
this repo to have a quick way to install the JDK from Oracle without going
through the Orcale webiste.

##### Files:
`jdk-8u73-linux-x64.rpm`

`README`

#### Possible Solution with a future JPM
Maybe there is room for a Java Package Manager(JPM) for installation of the JDK and
the Server JRE, inspired by `Ruby Gems`, `NPM`, or `Bower`, `PIP`, `Pear`. If there is a
better way, feel free to [let me know](mailto:claytonbarnette.com).



**Note:** *All right reserved to their respective owners, repo only used for personal installation of JDK, no other files are associated with this repo, outside of this README.*

*If you find yourself creating a solution to this problem, let me know.*
